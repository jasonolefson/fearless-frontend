import React from 'react';
import logo from './logo.svg';
import './App.css';
import AttendeesList from './AttendeesList';
import Nav from './Nav';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import {BrowserRouter, Route, Routes} from "react-router-dom"

function App(props) {
  if (props.attendees === undefined ) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
      <Routes>
        <Route path="locations">
          <Route path="new" element={<LocationForm/>} />
        </Route>
      </Routes>
        {/* <LocationForm /> */}
        {/* <AttendeesList attendees={props.attendees} /> */}
        {/* <ConferenceForm /> */}
      </div>
    </BrowserRouter>
  );
}


export default App;
