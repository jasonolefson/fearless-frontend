import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {locations: []};
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        //this.varname = this.varname.bind(this) <---- GOES HERE (ex)
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}
        data.max_presentations = data.maxPresentations;
        data.max_attendees = data.maxAttendees
        console.log(data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json()
            console.log(newConference)

            const cleared = {
                name: '',
                starts: '',
                ends: '',
                maxPresentations: '',
                maxAttendees: '',
                description: '',
                location: '',

            };
            this.handleNameChange = this.handleNameChange.bind(this);
             this.handleLocationChange = this.handleLocationChange.bind(this);
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';
    
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();
          this.setState({locations: data.locations})
          }
      }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} placeholder="Name" required type="text" id="name" name="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input placeholder="start date" required type="date" id="starts" name="starts" className="form-control"/>
                    <label htmlFor="starts">Start Date</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input placeholder="end date" required type="date" id="ends" name="ends" className="form-control"/>
                    <label htmlFor="ends">End Date</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input placeholder="Maximum Presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control"/>
                    <label htmlFor="max_presentations">Maximum Presentations</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input placeholder="Maximum Attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control"/>
                    <label htmlFor="max_attendees">Maximum Attendees</label>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="exampleFormControlTextarea1" className="form-label" id="description" name="description">Description</label>
                    <textarea className="form-control" id="description" name="description" rows="3"></textarea>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleLocationChange} required id="location" name="location" className="form-select">
                      <option value="">Choose a location</option>
                      {this.state.locations.map(location => {
                    return(
                        <option key={location.href} value={location.name}>
                            {location.name}
                        </option>
                        );
                  })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        )
    }
}

export default ConferenceForm