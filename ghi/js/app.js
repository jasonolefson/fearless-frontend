function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
    <div class="col-3">
      <div class="card shadow-sm  mb-3 bg-red rounded"">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h6 class="card-title font-weight-bold">${name}</h6>
          <h6 class="card-subtitle text-muted">${location}</h6>
          <p class="card-text text-muted">${description}</p>
        </div>
        <div class="card-footer">"${new Date(starts).toLocaleDateString()} - ${new Date(ends).toLocaleDateString()}"</div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    console.log("working!")
    try {
        const response = await fetch(url);

        if(!response.ok) {
            //figure out what to do when the response is bad
            throw new Error('Something went wrong!')
        } else {
            const data = await response.json();
            console.log(data);

            
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                  const details = await detailResponse.json();
                  const name = details.conference.name;
                  const description = details.conference.description;
                  //starts and ends
                  const starts = details.conference.starts;
                  const ends = details.conference.ends;
                  const location = details.conference.location.name
                  const pictureUrl = details.conference.location.picture_url;
                  const html = createCard(name, description, pictureUrl, starts, ends, location);
                  const column = document.querySelector('.card-group');
                  column.innerHTML += html;
                }


            //COMMENT OUT FOR CLARITY
            //
            // const conference = data.conferences[0];
            // const nameTag = document.querySelector('.card-title');
            // nameTag.innerHTML = conference.name;
            // console.log(conference)

            // const detailUrl = `http://localhost:8000${conference.href}`;
            // const detailResponse = await fetch(detailUrl);
            // if (detailResponse.ok) {
            // const details = await detailResponse.json();
            // console.log(details);

            // const detailSelector = document.querySelector('.card-text')
            // detailSelector.innerHTML = details.conference.description;

            // //picture
            // const picture = document.querySelector('.card-img-top')
            // picture.src = details.conference.location.picture_url;
            // console.log(html)
            //
            //COMMENT OUT FOR CLARITY
      }
        }
    } catch (e) {
        //figure out what to do if an error is raised
        console.log("may be error! (not rly)")
    }

});

