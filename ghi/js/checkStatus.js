// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get("jwt_access_payload")
if (payloadCookie) {
  // The cookie value is a JSON-formatted string, so parse it

  //BELOW BREAKS CODE
//   const encodedPayload = JSON.parse(payloadCookie.value);
//END COMMENT

  // Convert the encoded payload from base64 to normal string
  const decodedPayload = atob(payloadCookie.value)

  // The payload is a JSON-formatted string, so parse it
  const payload = JSON.parse(decodedPayload)

  // Print the payload
  console.log(payload.user.perms);


  // Check if "events.add_conference" is in the permissions.
  // If it is, remove 'd-none' from the link
  const perms = payload.user.perms
  for (let item of perms) {
    if (item === "events.add_conference") {
        const confTag = document.getElementById('conf');
        confTag.classList.remove("d-none");
    }
  }

  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the link
  for (let i of perms) {
    if (i === "events.add_location") {
        const confTag = document.getElementById('loc');
        confTag.classList.remove("d-none")
    }
  }
}